import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AdminService } from 'src/app/services/admin.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import Swal from 'sweetalert2';

interface Person {
  key: string;
  firstName: String,
  lastName: String,
  email: String,
  role:String,
  blocked: Boolean
}

@Component({
  selector: 'app-admin-list',
  templateUrl: './admin-list.component.html',
  styleUrls: ['./admin-list.component.scss']
})
export class AdminListComponent implements OnInit {

  isVisible = false;
  validateForm!: FormGroup;
  isSubmitted = false;
  isAdmin = true;


  listOfData: Person[] = [];
  admin : {
    role : String
  };
  selectedOption : '';
  listOfOption: string[] = ['Super Admin','Report Admin','Operation Admin','Account Admin'];

  constructor(private fb: FormBuilder,
    private adminService : AdminService,
    private authService: AuthService,
    private router : Router) { }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      firstName : [null, [Validators.required]],
      lastName : [null, [Validators.required]],
      email: [null, [Validators.email, Validators.required]],
    });

    this.getAdmin();
    this.adminList();
  }

  getAdmin(){
    this.adminService.getCurrentAdmin().subscribe(
      (success: any) => {
        this.admin = success.body.admin[0];
        console.log(this.admin);
        if(this.admin.role != 'Super Admin'){
          this.isAdmin = false
        }
        // this.loader.stop();
      },
      error => {
        // this.loader.stop();
      }
    );
  }

  adminList(){
    // this.loader.start();
    this.adminService.getAdminList().subscribe(
      (success: any) => {
        this.listOfData = success.body.admins;
        console.log(this.listOfData);
        // this.loader.stop();
      },
      error => {
        // this.loader.stop();
      }
    );
  }

  addAdmin(){
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }
    console.log(this.validateForm.value);
    if(this.validateForm.valid){
      let registerData = this.validateForm.value;
      registerData.role = this.selectedOption;
      // this.loader.start();
      this.authService.register(registerData).subscribe(
        (success: any) => {
          this.adminList();
          Swal.fire("Admin Added","New Admin Added","success");
          // this.loader.stop();
        },
        error => {
          // this.loader.stop();
        }
      );
      this.validateForm.reset();
      this.isSubmitted = false;
    }
    else{
      Swal.fire("Enter Complete Information","","warning");
    }
  }

  showModal(): void {
    this.isVisible = true;
  }

  handleOk(): void {
    this.addAdmin();
    this.isVisible = false;
    this.validateForm.reset();
  }

  handleCancel(): void {
    this.isVisible = false;
    this.validateForm.reset();
  }

  deleteConfirm(id): void {
    this.adminService.deleteAdmin(id).subscribe(
      (success: any) => {
        this.adminList();
        Swal.fire("Admin Deleted","Admin record is no more!!!","success");
        // this.loader.stop();
      },
      error => {
        Swal.fire("Error in deleting admin!!!","","warning");
        // this.loader.stop();
      }
    );
  }

  deleteCancel(): void {

  }
  blockConfirm(id,isBlocked): void {
    console.log(isBlocked);
    const data = {
      id : id,
      isBlocked: isBlocked
    }
    this.adminService.blockUnblock(data).subscribe(
      (success: any) => {
        console.log(success);
        if(success.body.admin.blocked){
          Swal.fire("Admin Unblocked","Admin can perform all actions!","success");
        }else{
          Swal.fire("Admin Blocked","Admin no longer perform some actions!","success");
        }
        this.adminList();
        // this.loader.stop();
      },
      error => {
        Swal.fire("Error in block/unblock admin!","","warning");
        // this.loader.stop();
      }
    );
  }

  blockCancel(): void {

  }


}
