import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { AdminRoutingModule } from './admin-routing.module';
import { AdminListComponent } from './admin-list/admin-list.component';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { AdminService } from '../services/admin.service';
import { AuthService } from '../services/auth/auth.service';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';

@NgModule({
    imports: [
        CommonModule,
        AdminRoutingModule,
        NzTableModule,
        NzDividerModule,
        NzIconModule,
        NzButtonModule,
        NzModalModule,
        FormsModule,
        ReactiveFormsModule,
        NzFormModule,
        NzInputModule,
        NzSelectModule,
        NzPopconfirmModule
    ],
    declarations: [
        AdminListComponent
    ],
    providers: [
        AdminService,
        AuthService
    ],
})

export class AdminModule { }