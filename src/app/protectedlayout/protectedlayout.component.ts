import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth/auth.service';

@Component({
  selector: 'app-protectedlayout',
  templateUrl: './protectedlayout.component.html',
  styleUrls: ['./protectedlayout.component.scss']
})
export class ProtectedlayoutComponent implements OnInit {

  isUserName = false;
  username : String;

  constructor(
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    if(this.authService.isAuthenticated()){
      this.isUserName = true;
      this.username = localStorage.getItem("username");
    }
  }

  logout(){
    this.authService.logout();
  }

}
