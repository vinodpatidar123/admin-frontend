import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError,retry,map } from 'rxjs/operators';
import { BaseService } from './auth/base.service';
import { ErrorHandlerService } from './auth/error-handler.service';

@Injectable()

export class AdminService {

    admin_base_url: string;
    headers = new HttpHeaders().set('Content-Type', 'application/json');
    
    constructor(
      private http: HttpClient,
      public router: Router,
      private bs: BaseService,
      private errorHandler: ErrorHandlerService
    ) {
      this.admin_base_url = bs.admin_base_url;
    }
  
    getAdminList() {
      return this.http.get(this.admin_base_url + "admins", { observe: "response" }).pipe(
        retry(3),
        catchError(this.errorHandler.handleError)
      );
    }

    getCurrentAdmin() {
      return this.http.get(this.admin_base_url + "admin", { observe: "response" }).pipe(
        retry(3),
        catchError(this.errorHandler.handleError)
      );
    }

    adminDetail(id){
      return this.http.get(this.admin_base_url + "admin/"+id, { observe: "response" }).pipe(
        retry(3),
        catchError(this.errorHandler.handleError)
      );
    }

    blockUnblock(data){
      return this.http.post(this.admin_base_url + "blockUnblockAdmin",data, { observe: "response" }).pipe(
        retry(3),
        catchError(this.errorHandler.handleError)
      );
    }

    deleteAdmin(id){
      return this.http.delete(this.admin_base_url + "admin/"+id, { observe: "response" }).pipe(
        retry(3),
        catchError(this.errorHandler.handleError)
      );
    }
}