import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class BaseService {

  // on local
  // public admin_base_url: string = ' http://127.0.0.1:3000/api/';

  // on live
  public admin_base_url ='https://adminapp-api.herokuapp.com/api/';

  constructor(private http: HttpClient) { }
}
