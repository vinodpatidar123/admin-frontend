import { Routes ,RouterModule } from '@angular/router';
//Route for content layout with sidebar, navbar and footer.

export const ProtectedRoutes: Routes = [
  {
    path: '',
    loadChildren: () => import('../admin/admin.module').then(m => m.AdminModule)
  }
];