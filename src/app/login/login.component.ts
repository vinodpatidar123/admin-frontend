import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { AuthService } from '../services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  validateForm!: FormGroup;
  passwordVisible = false;
  isSubmitted = false;

  constructor(private fb: FormBuilder,
    private router: Router,
    private authService: AuthService) {}


  ngOnInit(): void {
    this.validateForm = this.fb.group({
      email: [null, [Validators.required,Validators.email]],
      password: [null, [Validators.required]]
    });
  }

  submitForm(): void {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }
    console.log(this.validateForm.value);
    if(this.validateForm.valid){
      let loginData = this.validateForm.value;
      // this.loader.start();
      this.authService.login(loginData).subscribe(
        (success: any) => {
          localStorage.setItem("token", success.headers.get("Authorization"));
          console.log(success);
          localStorage.setItem("username", success.body.user.firstName);
          this.router.navigateByUrl(`/admin/adminlist`);
          Swal.fire("Logged In","Welcome to Dashbord","success");
          // this.loader.stop();
        },
        error => {
          // this.loader.stop();
        }
      );
      this.validateForm.reset();
      this.isSubmitted = false;
    }
    else{
      Swal.fire("Enter Complete Information","","warning");
    }
  }

}
