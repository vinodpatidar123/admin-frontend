import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ProtectedlayoutComponent } from './protectedlayout/protectedlayout.component';
import { ProtectedRoutes } from './routes/protected.routes';
import { AuthGuard } from './services/auth/auth-guard.service';


const routes: Routes = [
  {
    path: '',
    component: LoginComponent ,
    data: {
      title: 'Login'
    }
  },
  {
    path: 'admin',
    component: ProtectedlayoutComponent,
    children: ProtectedRoutes,
    data: {
      title: 'Admin List'
    },
    canActivate : [AuthGuard]
    },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
